So you want to keep on top of all the changes that happen to the main SUCS site repo in your own fork?

Good, keep reading.

cd to your cloned repo and run

    $ git remote -v

You should see something like this:

```
imranh@silver:~/public_html/sucs-site$ git remote -v
origin	git@projects.sucs.org:imranh/sucs-site.git (fetch)
origin	git@projects.sucs.org:imranh/sucs-site.git (push)
```

Now you want to run this

    $ git remote add upstream git@projects.sucs.org:sucssite/sucs-site.git

Then run

    git fetch upstream

Switch to the master branch (git branch master) and run the following to sync upstream changes into your branch

    git merge upstream/master


Enjoy :)