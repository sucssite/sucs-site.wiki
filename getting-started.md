To get started with editing the SUCS Website just follow these instructions:

Go to http://projects.sucs.org/sucssite/sucs-site and click Fork.

Log into silver and cd to your public_html folder (if you don't have one, make one) and run

    git clone http://projects.sucs.org/<username>/sucs-site.git

Then cd into the newly cloned repo (cd sucs-site) and run

    ./setup.sh



Check out http://githowto.com/setup for a tutorial on using git


If the website looks a little broken it means you don't have permission to access the beta db. Send an email to devel@lists.sucs.org asking for permission.